/* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row
    return '<tr>'+
            '<td rowspan="7">' + 
            'Applicant 1:' + d.applicant1 + '<br/>'+
            'Date of birth' + d.dateofbirth1 + '<br/>'+
            'Case refernce: ' + d.casereference + '<br/>'+
            'Cover: ' + d.cover + '</td>'+
    '</tr>';
}

$(document).ready(function(){
    var table = $('#policiesTable').DataTable( {
        "ajax": "../../Data/policies-table-data.txt",
        "columns": [
            
            { "data": "appref" },
            { "data": "client" },
            { "data": "abbr" },
            { "data": "status" },
            { "data": "lastchange" },
            { "data": "notes" },
            {
                "className":      'view',
                "orderable":      true,
                "data":           null,
                "defaultContent": '<button type="button" class="btn btn-default" aria-haspopup="true" aria-expanded="false"><span class="icon icon-arrow-1-down" aria-hidden="true"></span><span class="sr-only">Select to expand</span></button>'
            },
        ],
        "order": [[1, 'asc']]
    } );

    $('#policiesTable tbody').on('click', 'td.view', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
});