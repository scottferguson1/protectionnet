// If javascript is avaiable then add a class to control noscript messages 
document.body.className += ' js-enabled';

var nav = document.querySelector('.header-nav-toggle'); var toggleState = function (elem, one, two) { var elem = document.querySelector(elem); elem.setAttribute('data-state', elem.getAttribute('data-state') === one ? two : one); nav.setAttribute('aria-expanded', nav.getAttribute('aria-expanded') === one ? two : one); }; toggleState('#headerNav', 'false', 'true'); nav.onclick = function (e) { toggleState('#headerNav', 'false', 'true'); e.preventDefault(); };