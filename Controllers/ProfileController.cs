using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stormpath.SDK.Account;
using Stormpath.SDK.Client;

namespace ProtectionNET.Controllers
{
    // This API controller is protected and requires the user to be logged in.
    // For authorization policies, see the comments in Startup.
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly IClient stormpathClient;
        private readonly IAccount stormpathAccount;

        public ProfileController(IClient stormpathClient, Lazy<IAccount> stormpathAccount)
        {
            // Stormpath request objects injected via DI
            this.stormpathClient = stormpathClient;
            this.stormpathAccount = stormpathAccount.Value;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
    }
}